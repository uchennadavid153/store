export interface Store {
    img_url:string;
    rating:Rating;
    description:string;
    price:number;
    category:string;
    title:string;
    id:string | number ;
}

export interface Rating {
    rate:number;
    count:number
}