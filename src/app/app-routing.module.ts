import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefualtComponent } from './components/defualt/defualt.component';

const routes: Routes = [
  {
    path:'', component:DefualtComponent
  },
  {path:"**", redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
