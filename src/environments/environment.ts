// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'sstore-app',
    appId: '1:475274538136:web:d77f130a941e0d4b7e74ed',
    storageBucket: 'sstore-app.appspot.com',
    apiKey: 'AIzaSyAa78_RWWehiBX9NfyC4qaYwymMdBJHRf4',
    authDomain: 'sstore-app.firebaseapp.com',
    messagingSenderId: '475274538136',
    measurementId: 'G-NTZDWN3RBZ',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
